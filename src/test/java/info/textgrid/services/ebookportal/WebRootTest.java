package info.textgrid.services.ebookportal;

import io.quarkus.test.junit.QuarkusTest;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@Disabled("Disabled untils quarkus test failure understood")
@QuarkusTest
class WebRootTest {
    @Test
    void testWebRoot() {
        given()
          .when().get("/")
          .then()
             .statusCode(200);
    }

    @Test
    void testQuery() {
        given()
          .when().get("/?q=alice")
          .then()
             .statusCode(200);
    }

}