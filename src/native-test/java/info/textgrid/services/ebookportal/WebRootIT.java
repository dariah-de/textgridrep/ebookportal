package info.textgrid.services.ebookportal;

import io.quarkus.test.junit.QuarkusIntegrationTest;

@QuarkusIntegrationTest
class WebRootIT extends WebRootTest {
    // Execute the same tests but in packaged mode.
}
