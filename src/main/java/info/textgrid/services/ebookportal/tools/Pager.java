package info.textgrid.services.ebookportal.tools;


public class Pager {

  private int hits, limit, start, end, currentPage, totalPages;

  private int pageRange = 8; // Default page range (max amount of page links to be displayed at once).
  private Integer[] pages;

  public void calculatePages() {
    // Set currentPage, totalPages and pages.
    currentPage = (hits / limit) - ((hits - start) / limit) + 1;
    totalPages = (hits / limit) + ((hits % limit != 0) ? 1 : 0);
    int pagesLength = Math.min(pageRange, totalPages);
    pages = new Integer[pagesLength];

    if(start + limit <= hits) {
      end = start + limit;
    } else {
      end = hits;
    }

    // firstPage must be greater than 0 and lesser than totalPages-pageLength.
    int firstPage = Math.min(Math.max(0, currentPage - (pageRange / 2)), totalPages - pagesLength);

    // Create pages (page numbers for page links).
    for (int i = 0; i < pagesLength; i++) {
      pages[i] = ++firstPage;
    }

  }

  public int getLimit() {
    return limit;
  }

  public Pager setLimit(int limit) {
    this.limit = limit;
    return this;
  }

  public int getStart() {
    return start;
  }

  public Pager setStart(int start) {
    this.start = start;
    return this;
  }

  public int getHits() {
    return hits;
  }

  public Pager setHits(int hits) {
    this.hits = hits;
    return this;
  }

  public Integer[] getPages() {
    return pages;
  }

  public int getTotalPages() {
    return totalPages;
  }

  public int getEnd() {
    return end;
  }

  public int getCurrentPage() {
    return currentPage; 
  }

  public int getStartForLastPage() {
    return (totalPages - 1) * limit;
  }

}
