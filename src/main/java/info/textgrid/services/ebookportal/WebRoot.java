package info.textgrid.services.ebookportal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.fasterxml.jackson.jakarta.rs.json.JacksonJsonProvider;

import info.textgrid.namespaces.middleware.tgsearch.FacetType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.portal.Project;
import info.textgrid.namespaces.middleware.tgsearch.portal.ProjectsResponse;
import info.textgrid.services.ebookportal.services.SearchClientService;
import info.textgrid.services.ebookportal.tools.Pager;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import jakarta.inject.Inject;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.MediaType;

@Path("/")
public class WebRoot {

    @Inject
    SearchClientService searchClient;

    @Inject
    Template index;

    @ConfigProperty(name = "textgrid.aggregator.url")
    String aggregatorUrl;

    @ConfigProperty(name = "textgrid.search.url")
    String searchUrl;

    // final static String CONTENT_FILTER = "text/tg.edition+tg.aggregation+xml";
    final static String CONTENT_FILTER = "text/xml";

    @GET
    @Produces(MediaType.TEXT_HTML)
    public TemplateInstance hello(
            @QueryParam("q") String query,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("4") int limit) {

        String[] contentFilter = { "format:" + CONTENT_FILTER };

        Response searchResponse = null;
        Pager pager = null;
        if (query != null) {
            searchResponse = searchClient.searchQuery()
                    .setQuery(query)
                    .setFilterList(Arrays.asList(contentFilter))
                    .setLimit(limit)
                    .setStart(start)
                    .execute();

            pager = new Pager()
                    .setHits(Integer.parseInt(searchResponse.getHits()))
                    .setLimit(limit)
                    .setStart(start);

            pager.calculatePages();
        }

        return index.data("query", query)
                .data("searchResponse", searchResponse)
                .data("aggregatorUrl", aggregatorUrl)
                .data("pager", pager);
    }

}
