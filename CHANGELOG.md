
<a name="0.1.6"></a>
## [0.1.6](https://gitlab.gwdg.de/dariah-de/textgridrep/ebookportal/compare/0.1.5...0.1.6) (2024-07-11)

### Bug Fixes

* add tls hosts to helmchart


<a name="0.1.5"></a>
## [0.1.5](https://gitlab.gwdg.de/dariah-de/textgridrep/ebookportal/compare/0.1.4...0.1.5) (2024-07-11)


<a name="0.1.4"></a>
## [0.1.4](https://gitlab.gwdg.de/dariah-de/textgridrep/ebookportal/compare/0.1.3...0.1.4) (2024-07-11)

### Bug Fixes

* enable tls in helmchart


<a name="0.1.3"></a>
## [0.1.3](https://gitlab.gwdg.de/dariah-de/textgridrep/ebookportal/compare/0.1.2...0.1.3) (2024-07-04)

### Bug Fixes

* ingress controller in helm chart


<a name="0.1.2"></a>
## [0.1.2](https://gitlab.gwdg.de/dariah-de/textgridrep/ebookportal/compare/0.1.1...0.1.2) (2024-07-03)

### Bug Fixes

* adapt design for smaller screens


<a name="0.1.1"></a>
## [0.1.1](https://gitlab.gwdg.de/dariah-de/textgridrep/ebookportal/compare/0.1.0...0.1.1) (2024-07-03)


<a name="0.1.0"></a>
## 0.1.0 (2024-07-03)

### Features

* prototype of an e-ink paper optimized textgridrep portal

